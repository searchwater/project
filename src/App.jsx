import { Navbar, Header, Footer, Pricing, About, ContactForm } from "./components/";

export const App = () => {
  return (
    <>
      <Navbar />
      <Header />
      <About />
      <Pricing />
      <ContactForm />
      <Footer />
    </>
  );
};
