export const Pricing = () => {
  return (
    <section className="py-5" id="servicios">
      <div className="container px-5 my-5">
        <div className="text-center mb-5">
          <h1 className="fw-bolder">Paga lo que necesites</h1>
          <p className="lead fw-normal text-muted mb-0">Consulta los mejores planes de búsqueda</p>
        </div>
        <div className="row gx-5 justify-content-center">
          <div className="col-lg-6 col-xl-4">
            <div className="card shadow shadow-lg border border-0 rounded rounded-5 mb-5 mb-xl-0">
              <div className="card-body p-5">
                <div className="small text-uppercase fw-bold text-muted">Básico</div>
                <div className="mb-3">
                  <span className="display-4 fw-bold">$299.00</span>
                  <span className="text-muted">/ USD</span>
                </div>
                <ul className="list-unstyled mb-4">
                  <li className="mb-2">
                    <i className="bi bi-check text-primary"></i>
                    <strong>2 Horas de búsqueda</strong>
                  </li>
                  <li className="mb-2">
                    <i className="bi bi-check text-primary"></i>5 drones
                  </li>
                  <li className="mb-2">
                    <i className="bi bi-check text-primary"></i>
                    Territorios no montañosos
                  </li>
                  <li className="mb-2">
                    <i className="bi bi-check text-primary"></i>
                    Mapeo básico de lo encnotrado
                  </li>
                </ul>
                <div className="d-grid">
                  <a className="btn btn-outline-primary" href="#!">
                    Ver plan
                  </a>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-6 col-xl-4">
            <div className="card shadow shadow-lg border border-0 rounded rounded-5 mb-5 mb-xl-0">
              <div className="card-body p-5">
                <div className="small text-uppercase fw-bold">
                  <i className="bi bi-star-fill text-primary">Profesional</i>
                </div>
                <div className="mb-3">
                  <span className="display-4 fw-bold">$499.00</span>
                  <span className="text-muted">/ USD</span>
                </div>
                <ul className="list-unstyled mb-4">
                  <li className="mb-2">
                    <i className="bi bi-check text-primary"></i>
                    <strong>3 horas de búsqueda</strong>
                  </li>
                  <li className="mb-2">
                    <i className="bi bi-check text-primary"></i>
                    10 drones
                  </li>
                  <li className="mb-2">
                    <i className="bi bi-check text-primary"></i>
                    Territorios montañosos y áridos
                  </li>
                  <li className="mb-2">
                    <i className="bi bi-check text-primary"></i>
                    Mapeo avanzado de lo encnotrado
                  </li>
                </ul>
                <div className="d-grid">
                  <a className="btn btn-primary" href="#!">
                    Ver plan
                  </a>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-6 col-xl-4">
            <div className="card shadow shadow-lg border border-0 rounded rounded-5 mb-5 mb-xl-0">
              <div className="card-body p-5">
                <div className="small text-uppercase fw-bold">
                  <i className="bi bi-star-fill text-warning">Garantía</i>
                </div>
                <div className="mb-3">
                  <span className="display-4 fw-bold">$999.00</span>
                  <span className="text-muted">/ USD</span>
                </div>
                <ul className="list-unstyled mb-4">
                  <li className="mb-2">
                    <i className="bi bi-check text-primary"></i>
                    <strong>6 horas de búsqueda </strong>
                  </li>
                  <li className="mb-2">
                    <i className="bi bi-check text-primary"></i>
                    20 drones
                  </li>
                  <li className="mb-2">
                    <i className="bi bi-check text-primary"></i>
                    Cualquier territorio
                  </li>
                  <li className="mb-2">
                    <i className="bi bi-check text-primary"></i>
                    Mapeo completo de lo encnotrado en VR
                  </li>
                </ul>
                <div className="d-grid">
                  <a className="btn btn-primary" href="#!">
                    Ver plan
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="container" style={{ height: "50%" }}>
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="d-block w-100" src="https://www.ecoticias.com/wp-content/uploads/2022/01/106-10.jpg" alt="Third slide" />
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://estepais.com/wp-content/uploads/2021/07/paisajes-riparios-tita-elizondo.jpg" alt="Second slide" />
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://www.elagoradiario.com/wp-content/uploads/2020/12/latinoamerica-no-planet-b-latino-summit-1140x600.jpg" alt="Third slide" />
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </section>
  );
};
