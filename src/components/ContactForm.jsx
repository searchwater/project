export const ContactForm = () => {
  return (
    <section id="contacto">
      <div className="container">
        <div className="row justify-content-center">
          <div className="card rounded rounded-5 border border-0 shadow shadow-lg m-5 p-5">
            <div className="mx-5 px-5">
              <div className="col-md-12 my-3">
                <h2 className="text-center fw-bold">Contáctanos</h2>
              </div>
              <div className="col-md-12 px-5 mx-5 my-5">
                <form>
                  <div className="form-group mx-5 px-5">
                    <label htmlFor="" className="mb-2 text-muted">
                      Nombre
                    </label>
                    <input type="text" className="form-control" placeholder="Nombre" />
                  </div>
                  <div className="form-group my-3 mx-5 px-5">
                    <label htmlFor="" className="mb-2 text-muted">
                      Mensaje
                    </label>
                    <textarea type="text" className="form-control" placeholder="Escríbenos un mensaje" rows={10} />
                  </div>
                  <div className="mx-5 px-5">
                    <button className="btn btn-primary form-control">Enviar</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
