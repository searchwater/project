export const Header = () => {
  return (
    <header className="header-section bg-light py-5 overlay" id="inicio">
      <div className="container px-1">
        <div className="row gx-5 align-items-center justify-content-center">
          <div className="col-lg-8 col-xl-7 col-xxl-6">
            <div className="my-5 py-5 text-center overlay-content">
              <h1 className="display-5 fw-bolder text-black mb-4">Utiliza la IA para encontrar el vital líquido</h1>
              <p className="lead fw-normal text-black-50 mb-4">
                Con <strong>SEARCHWATER</strong> utilizamos drones programados con inteligencia artificial para detectar potenciales cuerpos de agua potable.
              </p>
              <div className="d-grid gap-3 d-sm-flex justify-content-center">
                <a className="btn btn-primary btn-lg px-4 me-sm-3" href="#nosotros">
                  Saber más
                </a>
                <a className="btn btn-outline-light btn-lg px-4" href="#servicios">
                  Servicios
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};
