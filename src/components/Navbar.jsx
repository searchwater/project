export const Navbar = () => {
  return (
    <nav className="navbar navbar-expand-lg py-3 navbar-light bg-light border border-bottom-3 shadow shadow-sm sticky-top">
      <div className="container-fluid">
        <a className="navbar-brand fw-bolder text-primary" href="#">
          SEARCHWATER
        </a>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <form>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <a className="nav-link active" aria-current="page" href="#">
                  Inicio
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link active" aria-current="page" href="#nosotros">
                  Nosotros
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link active" aria-current="page" href="#servicios">
                  Servicios
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link active" aria-current="page" href="#contacto">
                  Contacto
                </a>
              </li>
            </ul>
          </div>
        </form>
      </div>
    </nav>
  );
};
