export const Footer = () => {
  return (
    <footer class="page-footer font-small blue pt-4 bg-primary text-light">
      <div class="container text-center text-md-left">
        <div class="row d-flex justify-content-around">
          <div class="col-md-4 mt-md-0">
            <h5 class="text-uppercase fw-bold">SearchWater</h5>
            <hr />
            <p>Nuestra empresa es experta en la localización y búsqueda de agua subterránea.</p>
          </div>
          <div class="col-md-6 mb-md-0">
            <h5 class="text-uppercase fw-bold">Contáctanos</h5>
            <hr />
            <ul class="list">
              <li className="mb-2" style={{ listStyle: "none" }}>
                <a className="text-light text-decoration-none" href="#">
                  6181413614
                </a>
              </li>
              <li className="mb-2" style={{ listStyle: "none" }}>
                <a className="text-light text-decoration-none" href="#">
                  administración@searchwater.com
                </a>
              </li>
              <li className="mb-2" style={{ listStyle: "none" }}>
                <a className="text-light text-decoration-none" href="#">
                  Fraccionamiento alsuper #141
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div class="footer-copyright text-center py-3">
        Copyright © 2022
        <a className="btn btn-link text-light" href="/">
          searchwater.com
        </a>
      </div>
    </footer>
  );
};
