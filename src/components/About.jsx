export const About = () => {
  return (
    <section id="nosotros" className="container py-5" style={{ height: "100vh" }}>
      <div className="row">
        <div className="col-md-12 text-center">
          <div class="bg-light p-5 rounded rounded-2 shadow shadow-lg">
            <div class="card-body my-5">
              <h3 className="fw-bold">¿NECESITA BUSCAR AGUA SUBTERRÁNEA?</h3>
            </div>
            <div>
              <div class="row">
                <div class="col">
                  <div className="texto2 mt-5">
                    <p className="text-justify">Nuestra empresa es experta en la localización y búsqueda de agua de agua subterránea. Antes de la realización de un sondeo, es recomendable que profesionales evalúen la probabilidad de éxito del mismo. De este modo, se evita realizar sondeos fallidos, o de caudales de escasa importancia.</p>
                  </div>
                  <div className="boton mt-5 mb-5">
                    <a href="#contacto" class="btn btn-lg btn-primary">
                      Contáctanos
                    </a>
                  </div>
                </div>
                <div class="col">
                  <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQvSk8I_q_37cbIvQR3rsjFqaxqIq7D_7GBjg&usqp=CAU" width={"80%"} alt="" srcset="" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
